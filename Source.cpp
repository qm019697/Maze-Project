#include <iostream>
#include <conio.h> // for _getch()
#include "level.h"

using namespace std;
// the 2D array of chars needed to be declared globally as I cannot find a way to declare them in the header file withou them being static or there being a reinitialiation error
// they are declared as const chars, with a variable this_level which will hold the current level
const char O_level_map1[SIZE][SIZE] = { { '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' },
	{ '#','X','#',' ',' ',' ',' ','#','#','#',' ',' ',' ','#','#','#','#','#','#','#','#','#',' ',' ','#' },
	{ '#',' ',' ',' ','#',' ','#',' ',' ','#','#','#',' ','#','#','#','#','#',' ',' ',' ',' ',' ','#','#' },
	{ '#','#',' ','#','#',' ','#','#',' ','#','#','#',' ','#','#','#','#',' ',' ','#','#','#',' ',' ','#' },
	{ '#','#',' ',' ','#',' ',' ',' ',' ','#','#','#',' ',' ','#','#','#','#',' ','#','#','#','#',' ','#' },
	{ '#',' ',' ',' ','#','#','#','#',' ','#',' ',' ',' ','#','#','#','#','#',' ',' ','#','#','#',' ','#' },
	{ '#',' ','#',' ','#','#','#','#',' ',' ',' ','#','#','#','#','#','#','#','#',' ',' ','#','#',' ','#' },
	{ '#',' ','#',' ','#','#','#',' ',' ','#',' ',' ',' ',' ','#','#','#','#',' ',' ','#','#','#',' ','#' },
	{ '#',' ','#',' ',' ',' ',' ','#',' ',' ',' ','#','#','#','#',' ',' ',' ',' ','#','#','#',' ',' ','#' },
	{ '#',' ','#','#','#','#',' ',' ','#','#',' ',' ','#','#','#',' ','#','#','#','#','#',' ',' ','#','#' },
	{ '#',' ',' ',' ',' ','#','#',' ',' ','#','#',' ',' ',' ',' ',' ','#','#','#','#','#','#',' ',' ','#' },
	{ '#','#','#','#',' ',' ','#',' ',' ','#','#','#','#','#','#',' ',' ',' ','#','#','#',' ',' ','#','#' },
	{ '#','#','#',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ','#','#',' ','#','#','#','#',' ','#','#','#' },
	{ '#','#','#',' ','#','#',' ','#','#','#','#',' ','#',' ','#','#',' ','#',' ',' ',' ',' ',' ',' ','#' },
	{ '#','#','#',' ',' ','#',' ',' ',' ',' ',' ',' ','#',' ','#','#',' ',' ',' ','#','#','#',' ',' ','#' },
	{ '#',' ',' ',' ','#','#',' ',' ',' ',' ',' ',' ','#',' ','#','#','#','#',' ',' ',' ','#',' ','#','#' },
	{ '#',' ','#',' ','#','#',' ','#','#','#','#','#','#',' ','#','#','#','#',' ','#',' ','#',' ','#','#' },
	{ '#',' ','#',' ','#','#',' ','#','#','#',' ',' ',' ',' ','#','#','#',' ',' ','#',' ','#',' ',' ','#' },
	{ '#',' ','#',' ',' ',' ',' ','#','#',' ',' ','#',' ','#','#','#',' ',' ','#','#',' ','#',' ','#','#' },
	{ '#',' ','#',' ','#','#',' ',' ','#',' ','#','#',' ','#','#',' ',' ','#','#','#',' ','#',' ',' ','#' },
	{ '#',' ','#','#','#','#','#',' ',' ',' ','#','#',' ','#','#',' ',' ',' ',' ','#',' ','#','#',' ','#' },
	{ '#',' ','#','#','#','#','#','#','#','#','#','#',' ',' ',' ','#','#','#',' ','#',' ','#','#',' ','#' },
	{ '#',' ',' ',' ',' ',' ',' ',' ','#','#','#',' ',' ',' ','#','#','#',' ',' ','#',' ',' ','#','#','#' },
	{ '#','#','#','#','#',' ','#',' ','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ','O','#' },
	{ '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' } };


const char O_level_map2[SIZE][SIZE] = { { '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' },
	{ '#','X',' ',' ',' ',' ',' ','#','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ','#','#','#',' ',' ','#' },
	{ '#',' ',' ',' ','#',' ','#',' ','@','#','#','#',' ','#','#','#','#','#',' ',' ',' ',' ',' ','#','#' },
	{ '#',' ',' ','#','#',' ','#','#',' ','#','#','#',' ','#',' ',' ',' ',' ',' ','#',' ',' ',' ',' ','#' },
	{ '#',' ',' ',' ','#',' ',' ',' ',' ','#','#','#',' ',' ','#','#',' ',' ',' ','#','#',' ',' ',' ','#' },
	{ '#',' ',' ',' ','#','#','#','#',' ','#',' ',' ',' ',' ',' ',' ',' ','#',' ',' ','#','#','#',' ','#' },
	{ '#',' ',' ',' ','#',' ',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ','#','#',' ',' ','#','#',' ','#' },
	{ '#',' ','#',' ','#','#','#',' ',' ','#',' ',' ',' ',' ','#','#','#','#',' ',' ','#','#','#',' ','#' },
	{ '#',' ',' ',' ',' ',' ',' ','#',' ',' ',' ','#','#','#','#',' ',' ',' ',' ','#','#','#',' ',' ','#' },
	{ '#',' ','#','#','#','#',' ',' ','#','#',' ',' ','#','#','#',' ','#','#','#','#','#',' ',' ','#','#' },
	{ '#',' ',' ',' ',' ','#','#',' ',' ',' ','#',' ',' ',' ',' ',' ','#','#',' ',' ',' ','#',' ',' ','#' },
	{ '#','#',' ','#',' ',' ','#',' ',' ',' ','#','#','#','#','#',' ',' ',' ',' ',' ','#',' ',' ','#','#' },
	{ '#',' ','@',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ','#','#',' ','#','#','#','#',' ','#','#','#' },
	{ '#','#',' ',' ','#','#',' ','#','#','#','#',' ','#',' ','#','#',' ','#',' ',' ',' ',' ',' ',' ','#' },
	{ '#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ','#','#',' ',' ',' ','#','#','#',' ',' ','#' },
	{ '#',' ',' ',' ','#','#',' ',' ',' ',' ',' ',' ',' ',' ','#','#','#','#',' ',' ',' ','#',' ','#','#' },
	{ '#',' ',' ',' ',' ',' ',' ','#',' ','#',' ','#','#',' ',' ',' ',' ','#',' ','#',' ',' ',' ','#','#' },
	{ '#',' ','#',' ','#','#',' ','#',' ',' ',' ',' ','@',' ','#','#','#',' ',' ','#',' ',' ',' ',' ','#' },
	{ '#',' ','#',' ',' ',' ',' ','#','#',' ',' ','#',' ','#','#','#',' ',' ','#','#',' ',' ',' ','#','#' },
	{ '#',' ','#',' ','#','#',' ',' ','#',' ','#','#',' ','#','#',' ',' ','#','#','#',' ','#',' ',' ','#' },
	{ '#',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ','#','#',' ',' ',' ',' ','#',' ','#','#',' ','#' },
	{ '#',' ','#','#','#','#',' ',' ',' ',' ','#','#',' ',' ',' ','#','#','#',' ',' ',' ',' ',' ',' ','#' },
	{ '#',' ',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ','#','#','#',' ',' ','#',' ',' ','#','#','#' },
	{ '#','#','#','#','#',' ','#',' ','#','#',' ',' ','#',' ',' ',' ',' ',' ',' ',' ','#',' ',' ','O','#' },
	{ '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' } };

char this_level[SIZE][SIZE];


void bouncers(level level_info, int *P_bouncer1_x, int *P_bouncer1_y, int *P_bouncer2_x, int *P_bouncer2_y, int *P_bouncer3_x, int *P_bouncer3_y, int *Pincrement);
void bouncer_down(int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y);
void bouncer_up(int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y);
void bouncer_left(int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y);
void bouncer_right(int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y);

int main()
{
	level level_info;

	int *pX = &level_info.x,
		*pY = &level_info.y,
		*pLevel = &level_info.current_level,
		*pxS = &level_info.xS,
		*pyS = &level_info.yS,
		*pxP = &level_info.xP,
		*pyP = &level_info.yP,
		*P_bouncer1_x = &level_info.bouncer1_x,
		*P_bouncer1_y = &level_info.bouncer1_y,
		*P_bouncer2_x = &level_info.bouncer2_x,
		*P_bouncer2_y = &level_info.bouncer2_y,
		*P_bouncer3_x = &level_info.bouncer3_x,
		*P_bouncer3_y = &level_info.bouncer3_y,
		*Pincrement = &level_info.increment;
	bool *Pup = &level_info.up;
	char input;

	level_info.current_level = 1;
	level_info.set_current_level(O_level_map1, this_level);
	level_info.menu(this_level);

	while (level_info.current_level == 1)
	{		
		input = level_info.get_input();
		level_info.move(level_info, input, this_level, O_level_map1, pX, pY, pxS, pyS, pxP, pyP, pLevel, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
		level_info.locator(pX, pY, this_level);// locates the position of the X within the game
		if (level_info.is_level_complete(level_info) == true)
		{
			level_info.current_level++; // increments the level, consequently breaking the loop
		}
	}
	level_info.set_current_level(O_level_map2, this_level);
	level_info.start_new_level(level_info, this_level);
	level_info.set_bouncers(level_info, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
	while (level_info.current_level == 2)
	{
		level_info.input = level_info.get_input();
		level_info.move(level_info, level_info.input, this_level, O_level_map1, pX, pY, pxS, pyS, pxP, pyP, pLevel, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
		level_info.locator(pX, pY, this_level);
		bouncers(level_info, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
		level_info.is_dead(level_info, O_level_map2, this_level, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
		if (level_info.is_level_complete(level_info) == true)
		{
			level_info.current_level++;
		}
	}
	//level_info.set_current_level(level_map3, this_level);
	//level_info.start_new_level(this_level, level_info.current_level);
	level_info.game_complete();
	return 0;
}



void bouncers(level level_info, int *P_bouncer1_x, int *P_bouncer1_y, int *P_bouncer2_x, int *P_bouncer2_y, int *P_bouncer3_x, int *P_bouncer3_y, int *Pincrement)
{
	if (level_info.current_level == 2)
	{
		cout << "1" << level_info.bouncer1_x << " " << level_info.bouncer1_y << endl;
		cout << "2" << level_info.bouncer2_x << " " << level_info.bouncer2_y << endl;
		cout << "3" << level_info.bouncer3_x << " " << level_info.bouncer3_y << endl;
		if (level_info.increment == 1 || level_info.increment == 2 || level_info.increment == 3 || level_info.increment == 4 || level_info.increment == 5)
		{
			bouncer_down(level_info.bouncer1_x, level_info.bouncer1_y, P_bouncer1_x, P_bouncer1_y);
			bouncer_right(level_info.bouncer2_x, level_info.bouncer2_y, P_bouncer2_x, P_bouncer2_y);
			bouncer_down(level_info.bouncer3_x, level_info.bouncer3_y, P_bouncer3_x, P_bouncer3_y);
		}
		if (level_info.increment == 6 || level_info.increment == 7 || level_info.increment == 8 || level_info.increment == 9 || level_info.increment == 0)
		{
			bouncer_up(level_info.bouncer1_x, level_info.bouncer1_y, P_bouncer1_x, P_bouncer1_y);
			bouncer_left(level_info.bouncer2_x, level_info.bouncer2_y, P_bouncer2_x, P_bouncer2_y);
			bouncer_up(level_info.bouncer3_x, level_info.bouncer3_y, P_bouncer3_x, P_bouncer3_y);
		}
		*Pincrement = ((level_info.increment + 1) % 10); // will make sure that the increment returns a value between 0 and 9
	}
	
}



void bouncer_down(int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y)
{
	this_level[bouncer_x][bouncer_y] = ' ';
	this_level[bouncer_x + 1][bouncer_y] = '@';
	*P_bouncer_x = bouncer_x + 1;
}

void bouncer_up(int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y)
{
	this_level[bouncer_x][bouncer_y] = ' ';
	this_level[bouncer_x - 1][bouncer_y] = '@';
	*P_bouncer_x = bouncer_x - 1;
}

void bouncer_left (int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y)
{
	this_level[bouncer_x][bouncer_y] = ' ';
	this_level[bouncer_x][bouncer_y - 1] = '@';
	*P_bouncer_y = bouncer_y - 1;
}

void bouncer_right(int bouncer_x, int bouncer_y, int *P_bouncer_x, int *P_bouncer_y)
{
	this_level[bouncer_x][bouncer_y] = ' ';
	this_level[bouncer_x][bouncer_y + 1] = '@';
	*P_bouncer_y = bouncer_y + 1;
}

