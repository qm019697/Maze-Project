#pragma once
#define SIZE 25
//use a variable called current_level[SIZE][SIZE] to hold the current level - could make it easier to pass?
class level
{
public:
	level();
	~level();
	int current_level = 1 // this is the current level
		, x = 1
		, y = 1  // sets start position for player
		, xS = 0  // the following four variables are for the first enemy
		, yS = 0
		, xP = 0 // these four variables store information about another enemy
		, yP = 0
		, bouncer1_x,
		bouncer1_y,
		bouncer2_x,
		bouncer2_y,
		bouncer3_x,
		bouncer3_y,
		increment = 1;		
		// this creates a number of 'bouncers', that simply move up and down or side to side bouncing between obstacles
		//, lives = 3 // this stores the number of lives the player has remaining
		//, *pLives = &lives;
	bool up = true;
	bool bouncer_moved = false;
	bool bouncer_up = true;
	char input,
		enemy;
	void print_level(char level_map[SIZE][SIZE]); // clears screen and prints level to screen
	void reset_level(level level_info, const char O_level_map[SIZE][SIZE], char level_map[SIZE][SIZE], int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement); // this function 'resets' the level by setting all values equal to the const version of the level
	void menu(char level_map[SIZE][SIZE]); // displays menu with instructions at start
	bool is_level_complete(level level_info); // tests to see if the level is complete, returns true if complete
	char get_input(); // gets input for movement using _getch()
	void locator(int *x, int *y, char level_map[SIZE][SIZE]); // this function locates the position of the X within the arrays
	bool move(level level_info, char input, char level_map[SIZE][SIZE], const char O_level_map[SIZE][SIZE], int * pX, int * pY, int * pxS, int * pyS, int * pxP, int * pyP, int * pLevel, int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement); // this will only work for level one right now, will need to update for each level?
	void find_enemy(level level_info, int * xS, int * yS, char level_map[SIZE][SIZE]); // this function locates the position of the enemy $ within the arrays
	bool move_enemy(level level_info, int * pxS, int * pyS, char level_map[SIZE][SIZE]); // if this returns true then the char position is the same as the enemy, this will move after the player moves to ensure that the positioning is correct
	void start_new_level(level level_info, char level_map[SIZE][SIZE]);// this function is called at the end of each level and will start the next level
	void set_current_level(const char O_level_map[SIZE][SIZE], char level_map[SIZE][SIZE]); // this function sets the variable current level to the values of the constant chars that are globally declared
	void set_bouncers(level level_info, int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement); // this sets the bouncers to their original positions
	void is_dead(level level_info, const char O_level_map[SIZE][SIZE], char level_map[SIZE][SIZE], int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement); // this checks if the character is on top of one of the @ symbols or the or symbols
	void game_complete(); // this prints the game over message at the end

};

