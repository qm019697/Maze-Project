#include "level.h"
#include <iostream>
#include <conio.h>



using namespace std;

level::level()
{
	
}



level::~level()
{
}

void level::print_level(char level_map[SIZE][SIZE])
{
	try {
		system("CLS");
		for (size_t i = 0; i < SIZE; i++)
		{
			for (size_t j = 0; j < SIZE; j++)
			{
				cout << level_map[i][j];
			}
			cout << endl;
		}
	}
	catch (const std::exception&)
	{
		cout << "Error printing map" << endl;
	}

}

void level::reset_level(level level_info, const char O_level_map[SIZE][SIZE], char level_map[SIZE][SIZE], int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement)
{
	try {
		for (size_t i = 0; i < SIZE; i++)
		{
			for (size_t j = 0; j < SIZE; j++)
			{
				level_map[i][j] = O_level_map[i][j];
			}
		}
		set_bouncers(level_info, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
	}
	catch (const std::exception&)
	{
		cout << "Error resetting map and positions" << endl;
	}
}

void level::menu(char level_map[SIZE][SIZE])
{
	std::cout << std::endl << std::endl;
	std::cout << "\t\t\t Maze Game" << std::endl << std::endl;
	std::cout << "\tInstructions:" << std::endl;
	std::cout << "\tUse the 'w', 'a', 's', 'd' keys to move your character and escape the maze!" << std::endl;
	std::cout << "\tAvoid the enemies '�' and '$', get to the exit 'O' to complete the level" << std::endl; // instructions
	std::cout << "\tPress the space bar to start...";
	char go;

	while (1) // while loop to test input until input == ' '
	{
		go = _getch();
		if (go = ' ')
		{
			break;
		}
	}
	if (go == ' ')// when input == ' ' then the screen will clear and then the first level will be printed
	{
		print_level(level_map);
	}

}

bool level::is_level_complete(level level_info)
{
	if (level_info.x == (SIZE - 2) && level_info.y == (SIZE - 2)) // if position is final, the loop will complete
	{
		return true;
	}
	else
	{
		return false;
	}

}

char level::get_input()
{
	try {
		char input = 127; //gurantees that the output is not wasdto start
		while (1)
		{
			input = _getch();
			tolower(input); // converts the input to lowercase so there is no confusion
			if (input == 'w' || input == 'a' || input == 's' || input == 'd')
			{
				break;
			}
		}

		return input; // returns input
	}
	catch (const std::exception&)
	{
		cout << "Input not recieved..." << endl;
	}
	

}

void level::locator(int * x, int * y, char level_map[SIZE][SIZE])
{
	try
	{
		for (int i = 0; i < SIZE; i++) // i == x
		{
			for (int j = 0; j < SIZE; j++) //j == y
			{
				if (level_map[i][j] == 'X')
				{
					*x = i;
					*y = j;
					break; // breaks once the location of x is found
				}
			}
		}
	}
	catch (const std::exception&)
	{
		cout << "Player coordinates not located successfully..." << endl;
	}
	

}

bool level::move(level level_info, char input, char level_map[SIZE][SIZE], const char O_level_map[SIZE][SIZE] , int * pX, int * pY, int * pxS, int * pyS, int * pxP, int * pyP, int * pLevel, int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement)
{
	bool moved = false; //moved bool is set to false to start 
	if (input == 'w' && level_info.x != 0)// moves up if 'w'
	{
		if (level_map[level_info.x - 1][level_info.y] == ' ' || level_map[level_info.x - 1][level_info.y] == 'O')
		{
			level_map[level_info.x - 1][level_info.y] = 'X';
			level_map[level_info.x][level_info.y] = ' '; // sets previous position to ' '
			*pX = *pX - 1;
			moved = true;
		}
		else if (level_map[level_info.x - 1][level_info.y] == '$' || level_map[level_info.x - 1][level_info.y] == '�' || level_map[level_info.x - 1][level_info.y] == '@')
		{
			reset_level(level_info, O_level_map, level_map, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
			print_level(level_map);
		}
	}
	else if (input == 'a' && level_info.y != 0) //moves left if 'a'
	{
		if (level_map[level_info.x][level_info.y - 1] == ' ' || level_map[level_info.x][level_info.y - 1] == 'O')
		{
			level_map[level_info.x][level_info.y - 1] = 'X';
			level_map[level_info.x][level_info.y] = ' '; // sets previous position to ' '
			*pY = *pY - 1;
			moved = true;
		}
		else if (level_map[level_info.x][level_info.y - 1] == '$' || level_map[level_info.x][level_info.y - 1] == '�' || level_map[level_info.x][level_info.y - 1] == '@')
		{
			reset_level(level_info, O_level_map, level_map, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
			print_level(level_map);
		}
	}
	else if (input == 's' && level_info.x != (SIZE - 1)) //moves down if 's'
	{
		if (level_map[level_info.x + 1][level_info.y] == ' ' || level_map[level_info.x + 1][level_info.y] == 'O')
		{
			level_map[level_info.x + 1][level_info.y] = 'X';
			level_map[level_info.x][level_info.y] = ' ';// sets previous position to ' '
			*pX = *pX + 1;
			moved = true;
		}
		else if (level_map[level_info.x + 1][level_info.y] == '$' || level_map[level_info.x + 1][level_info.y] == '�' || level_map[level_info.x + 1][level_info.y] == '@')
		{
			reset_level(level_info, O_level_map, level_map, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
			print_level(level_map);
		}
	}
	else if (input == 'd' && level_info.y != (SIZE - 1)) // moves right if 'd'
	{
		if (level_map[level_info.x][level_info.y + 1] == ' ' || level_map[level_info.x][level_info.y + 1] == 'O')
		{
			level_map[level_info.x][level_info.y + 1] = 'X';
			level_map[level_info.x][level_info.y] = ' ';// sets previous position to ' '
			*pY = *pY + 1;
			moved = true;
		}
		else if (level_map[level_info.x][level_info.y + 1] == '$' || level_map[level_info.x][level_info.y + 1] == '�' || level_map[level_info.x][level_info.y + 1] == '@')
		{
			reset_level(level_info, O_level_map, level_map, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
			locator(pX, pY, level_map);
		}
	}

	if (moved == true) // if the character has moved this will trigger and new map wil print
	{
		print_level(level_map); // prints with new position of 'X'
		return moved;
	}
	else
	{
		return moved;
	}

}

void level::find_enemy(level level_info, int * xS, int * yS, char level_map[SIZE][SIZE])
{
	try
	{
		for (int i = 0; i < SIZE; i++) // i == x
		{
			for (int j = 0; j < SIZE; j++) //j == y
			{
				if (level_map[i][j] == level_info.enemy)
				{
					*xS = i;
					*yS = j;
					break; // breaks once the location is found
				}
			}
		}
	}
	catch (const std::exception&)
	{
		cout << "Coordinates of NPC's not located successfully" << endl;
	}
	
}

bool level::move_enemy(level level_info, int * pxS, int * pyS, char level_map[SIZE][SIZE])
{
	int random;
	random = rand() % 4; // random == 0 up, random == 1 down, random == 2 left, random == 3 right

	if (random == 0 && level_map[level_info.xS - 1][level_info.yS] == 'X')
	{
		return true;
	}
	else if (random == 0 && (level_map[level_info.xS - 1][level_info.yS] == ' '))
	{
		level_map[level_info.xS][level_info.yS] = ' ';
		level_map[level_info.xS - 1][level_info.yS] = enemy;
		return false;
	}
	else if (random == 1 && level_map[level_info.xS + 1][level_info.yS] == 'X')
	{
		return true;
	}
	else if (random == 1 && (level_map[level_info.xS + 1][level_info.yS] == ' '))
	{
		level_map[level_info.xS][level_info.yS] = ' ';
		level_map[level_info.xS + 1][level_info.yS] = enemy;
		return false;
	}
	else if (random == 2 && level_map[level_info.xS][level_info.yS - 1] == 'X')
	{
		return true;
	}
	else if (random == 2 && (level_map[level_info.xS][level_info.yS - 1] == ' '))
	{
		level_map[level_info.xS][level_info.yS] = ' ';
		level_map[level_info.xS][level_info.yS - 1] = enemy;
		return false;
	}
	else if (random == 3 && level_map[level_info.xS][level_info.yS + 1] == 'X')
	{
		return true;
	}
	else if (random == 3 && (level_map[level_info.xS][level_info.yS + 1] == ' '))
	{
		level_map[level_info.xS][level_info.yS] = ' ';
		level_map[level_info.xS][level_info.yS + 1] = enemy;
		return false;
	}
	else
	{
		return false;
	}

}

void level::start_new_level(level level_info, char level_map[SIZE][SIZE])
{
	system("cls");
	cout << std::endl << std::endl;
	cout << "\tWell done! Level " << level_info.current_level - 1 << " complete!" << endl;
	cout << "\tTo start level " << level_info.current_level <<  " press the spacebar..." << endl;
	
	char go;

	while (1) // while loop to test input until input == ' '
	{
		go = _getch();
		if (go = ' ')
		{
			break;
		}
	}
	if (go == ' ')// when input == ' ' then the screen will clear and then the first level will be printed
	{
		print_level(level_map);
	}
}

void level::set_current_level(const char O_level_map[SIZE][SIZE], char level_map[SIZE][SIZE])
{
	for (size_t i = 0; i < SIZE; i++)
	{
		for (size_t j = 0; j < SIZE; j++)
		{
			level_map[i][j] = O_level_map[i][j];
		}
	}
}



void level::set_bouncers(level level_info, int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement)
{
	if (level_info.current_level == 2) // if current level is 2, this will set the positions of all the bouncers to their original positions on the map, will need to be called anytime there is a reset level
	{
		*P_bouncer1_x = 2;
		*P_bouncer1_y = 8;
		*P_bouncer2_x = 12;
		*P_bouncer2_y = 2;
		*P_bouncer3_x = 17;
		*P_bouncer3_y = 12;
		*Pincrement = 1;
	}
}

void level::is_dead(level level_info, const char O_level_map[SIZE][SIZE], char level_map[SIZE][SIZE], int * P_bouncer1_x, int * P_bouncer1_y, int * P_bouncer2_x, int * P_bouncer2_y, int * P_bouncer3_x, int * P_bouncer3_y, int *Pincrement)
{
	if ((level_info.x == level_info.bouncer1_x && level_info.y == level_info.bouncer1_y) || (level_info.x == level_info.bouncer2_x && level_info.y == level_info.bouncer2_y) || (level_info.x == level_info.bouncer3_x && level_info.y == level_info.bouncer3_y))
	{
		reset_level(level_info, O_level_map, level_map, P_bouncer1_x, P_bouncer1_y, P_bouncer2_x, P_bouncer2_y, P_bouncer3_x, P_bouncer3_y, Pincrement);
		print_level(level_map);
	}
}

void level::game_complete()
{
	system("cls");
	cout << endl << endl;
	cout << "\tCongratulations, you have completed the game!" << endl;
	cout << "\tPress any key to exit...." << endl;
	char finish = _getch();
}





